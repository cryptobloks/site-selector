# Site Selector

Proof of concept client side endpoint selector:

- must take in path of request as script variable, and apend the path to the redirected uri.
- can configure each servoce endpoint to look for a specific query variable on home page access, and if not present, could run the fastest endpoint script
- can download and store the fastest endpoint script, and execute if page received 4xx/5xx errors.

- onLoad:

  - retirieve the endpoint list from APIs
    /libre/v1/getServiceEndpoints returns (based on producerjson records):

```
{
  "chainId": "38b1d7815474d0c60683ecbea321d723e83f5da6ae5f1c1f9fecc69d9ba96465",
  "availableServiceTypes": [
    "seed",
    "query",
    "libre-api",
    "libre-explorer",
    "libre-dashboard"
  ],
  "services": [
    {
      "serviceType": "seed",
      "nodes": [
        {
          "index": 0,
          "provider": "blokcrafters",
          "branding": {
            "logo_256": "https://blokcrafters.com/images/blokcrafters1_256.png",
            "logo_1024": "https://blokcrafters.com/images/blokcrafters1_1024.png",
            "logo_svg": "https://blokcrafters.com/images/blokcrafters_logo.svg"
          },
          "website": "https://blokcrafters.com",
          "p2pUrl": "libre-peer-eu.blokcrafters.io:9876"
        },
        {
          "index": 1,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "p2pUrl": "p2p.libre.iad.cryptobloks.io:9876"
        },
        {
          "index": 2,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "p2pUrl": "p2p.libre.pdx.cryptobloks.io:9876"
        },
        {
          "index": 3,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "p2pUrl": "p2p.libre.cryptobloks.io:9876"
        },
        {
          "index": 4,
          "provider": "cultureblock",
          "branding": {
            "logo_256": "https://cultureblock.io/assets/logo-sm.png",
            "logo_1024": "https://cultureblock.io/assets/logo-lg.png",
            "logo_svg": "https://cultureblock.io/assets/logo.svg"
          },
          "website": "https://cultureblock.io",
          "p2pUrl": "libre-mainnet-p2p.cultureblock.io:9874"
        },
        {
          "index": 5,
          "provider": "rioblocks",
          "branding": {
            "logo_256": "https://rioblocks.io/logo-rioblocks-white-256.png",
            "logo_1024": "https://rioblocks.io/logo-rioblocks-white-1024.png",
            "logo_svg": "https://rioblocks.io/logo-rioblocks-circle-whitebg.svg"
          },
          "website": "https://eosrio.io",
          "p2pUrl": "p2p.libre.rioblocks.io:8152"
        }
      ]
    },
    {
      "serviceType": "query",
      "nodes": [
        {
          "index": 0,
          "provider": "blokcrafters",
          "branding": {
            "logo_256": "https://blokcrafters.com/images/blokcrafters1_256.png",
            "logo_1024": "https://blokcrafters.com/images/blokcrafters1_1024.png",
            "logo_svg": "https://blokcrafters.com/images/blokcrafters_logo.svg"
          },
          "website": "https://blokcrafters.com",
          "serviceUrl": "http://libre.blokcrafters.io",
          "serviceUrlSecure": "https://libre.blokcrafters.io",
          "features": [
            "account-query",
            "chain-api"
          ]
        },
        {
          "index": 1,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://api.libre.iad.cryptobloks.io",
          "serviceUrlSecure": "https://api.libre.iad.cryptobloks.io",
          "features": [
            "chain-api",
            "account-query",
            "history-v1",
            "hyperion-v2"
          ]
        },
        {
          "index": 2,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://api.libre.pdx.cryptobloks.io",
          "serviceUrlSecure": "https://api.libre.pdx.cryptobloks.io",
          "features": [
            "chain-api",
            "account-query"
          ]
        },
        {
          "index": 3,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://api.libre.cryptobloks.io",
          "serviceUrlSecure": "https://api.libre.cryptobloks.io",
          "features": [
            "chain-api",
            "account-query",
            "history-v1",
            "hyperion-v2"
          ]
        },
        {
          "index": 4,
          "provider": "cultureblock",
          "branding": {
            "logo_256": "https://cultureblock.io/assets/logo-sm.png",
            "logo_1024": "https://cultureblock.io/assets/logo-lg.png",
            "logo_svg": "https://cultureblock.io/assets/logo.svg"
          },
          "website": "https://cultureblock.io",
          "serviceUrl": "http://libre-mainnet-api.cultureblock.io",
          "serviceUrlSecure": "https://libre-mainnet-api.cultureblock.io",
          "features": [
            "chain-api"
          ]
        }
      ]
    },
    {
      "serviceType": "libre-api",
      "nodes": [
        {
          "index": 0,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://api.libre.cryptobloks.io/libre",
          "serviceUrlSecure": "https://api.libre.cryptobloks.io/libre",
          "features": [
            "libre-api"
          ]
        }
      ]
    },
    {
      "serviceType": "libre-explorer",
      "nodes": [
        {
          "index": 0,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://explorer.libre.cryptobloks.io",
          "serviceUrlSecure": "https://explorer.libre.cryptobloks.io",
          "features": [
            "libre-explorer"
          ]
        }
      ]
    },
    {
      "serviceType": "libre-dashboard",
      "nodes": [
        {
          "index": 0,
          "provider": "cryptobloks",
          "branding": {
            "logo_256": "https://bp.cryptobloks.io/imgs/logo_256.png",
            "logo_1024": "https://bp.cryptobloks.io/imgs/logo_1024.png",
            "logo_svg": "https://bp.cryptobloks.io/imgs/logo.svg"
          },
          "website": "https://bp.cryptobloks.io",
          "serviceUrl": "http://dashboard.libre.cryptobloks.io",
          "serviceUrlSecure": "https://dashboard.libre.cryptobloks.io",
          "features": [
            "libre-dashboard"
          ]
        }
      ]
    }
  ]
}
```

List of endpoints dictated by endpoint listings in chain bp.json entries. API will consolidate like named entries between providers, allowing for new services to be rolled out without changing ANY core or endpoint code. Add to bp.json, upload to chain, data will start ot propogate.

- Once downloaded, endpoints are tested to find the best endpoints for the client based on liveness and response timen for test page access.
- Two fastest endpoints are injected in to the json file, along with the update timestamp. JSON it is persisted to client for future reference by code:

Persist dynamic json file stored on client:

````
        chain_id:
        services:
          coreApi:
            last_update:
            priUrl: (selected by code)
            secUrl: (selected by code)
            validHosts: [
              { index: #, provider_name: ".owner", provider_logo: ".logo_svg", provider_url: ".website", service_url: ".ssl_endpoint", services: ".features" }
            ]
          libreApi:
            last_update:
            priUrl: (selected by code)
            secUrl: (selected by code)
            validHosts: [
              { index: #, provider_name: ".owner", provider_logo: ".logo_svg", provider_url: ".website", service_url: ".ssl_endpoint", services: ".features" }
            ]
          chainApi:
            last_update:
            priUrl: (selected by code)
            secUrl: (selected by code)
            validHosts: [
              { index: #, provider_name: ".owner", provider_logo: ".logo_svg", provider_url: ".website", service_url: ".ssl_endpoint", services: ".features" }
            ]
          historyApi:
            last_update:
            priUrl: (selected by code)
            secUrl: (selected by code)
            validHosts: [
              { index: #, provider_name: ".owner", provider_logo: ".logo_svg", provider_url: ".website", service_url: ".ssl_endpoint", services: ".features" }
            ]
            ```

  - redirect client to the fastest node/endpoint.

  - periodically check API nodes for faster response times.  if active session, offer the user the option to swap to a faster endpoint.
  - if a failure is detected, identify if another endpoint is available, and switch transparently on failures.






  - app will select primary and secondary endpoints for all APIs based on performance.
  - Transactions will be sent to the primary endpoint unless a fault is identifed... in such a case, the backup will be invoked.
  - periodically, the endpoint list will be queried. if same, all endpoints will be re-validated. If new file, these endpoints will be validated, and the fastest 2 endpoints will be established for future transactions.

- apps will need to call a local library to provide the app with the correct endpoint type.

- use generation timestamp / expire/ttl to decide when to refresh the list from the API
- have all error messages execute endpoint selection code.



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

````

cd existing_repo
git remote add origin https://gitlab.com/cryptobloks/site-selector.git
git branch -M main
git push -uf origin main

```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/cryptobloks/site-selector/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
```
